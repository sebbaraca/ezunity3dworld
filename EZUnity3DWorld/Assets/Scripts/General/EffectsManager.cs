﻿using UnityEngine;
using System.Collections;
/*
*@author Juan Sebastian Baracaldo Angel
*This class is used to store all music FX. 
*If you change any sound here it will affect the whole project.
*/

public class EffectsManager : MonoBehaviour
{
    //-------------------------------
    // Variables
    //-------------------------------
    //The Instance of this manager
    public static EffectsManager instance;
    //Each of these Audioclip lists are for each audio FX
    //Each element of each list is a variation of the FX
    public AudioClip[] dashEffects;

    public AudioClip[] itemEffects;

    public AudioClip[] deathEffects;
    //The effect Types
    public enum EffectType
    {
        Dash,
        Items,
        Death
    };
    //-------------------------------
    //Methods
    //-------------------------------
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        
    }


    //Use this method to play any sound FX declared in this class.
    //The Effect Type is declared in an enum in this class
    //The audioSource is the AudioSource in which you want to play your sound. It's recomended to have one audio source for each FX if they can sound at the same time.
    //Volume is a parameter between 0 and 1 in which 0 is mute and 1 max volume
    //Priority Means which sounds should be priorized over others. This parameter comes between 0 and 256. 0 is high priority, and 0 is low
    
    public void playEffect(EffectType typeP, AudioSource audioSource,float volume, int priority)
    {
        //you can add or remove params if you want to. One example is the pitch parameter of the audio source which makes the audio faster or slower while changing the pitch too.
        //audioSource.pitch = pitch;
        audioSource.priority = priority;
        audioSource.volume = volume;
        

        switch (typeP)
        {
            case EffectType.Death:
                int x = (int)Random.Range(0, deathEffects.Length);
                audioSource.clip = deathEffects[x];
                audioSource.Play();
                break;
            case EffectType.Dash:
                int y = (int) Random.Range(0, dashEffects.Length);
                audioSource.clip = dashEffects[y];
                audioSource.Play();
                break;
            case EffectType.Items:
                int z = (int)Random.Range(0, itemEffects.Length);
                audioSource.clip = itemEffects[z];
                audioSource.Play();
                break;           

        }
    }
	
}
