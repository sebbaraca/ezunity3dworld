﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
*@author Juan Sebastian Baracaldo Angel
*This Script will be used to Manage global variables of the videoGame
*It is present on all scenes and is a singleton
*Some examples are the global player score, the list of levels,  the gamemodes, the gamespeed 
*/
public class GameManager : MonoBehaviour
{
    //--------------------------------------
    // Public Constants
    //--------------------------------------

    // Enum for the different game modes
    public enum TypeOfGameMode
    {
        type1,
        type2,
        type3
    }
    //Enum to know the playerId
    public enum PlayerId { Player1, Player2, Player3, Player4, Default };

    // Enum to know each character
    public enum CharacterId { Character1, Character2, Character3, Character4 };
    
    //-----------------------------------
    // Public Variables
    //-----------------------------------

    //Instance for singleton patern
    public static GameManager instance;

    //-----------------------------------
    // Private Variables
    //-----------------------------------

    // List of all the game modes that will be played in a session
    private TypeOfGameMode[] gameModes;

    public TypeOfGameMode[] GameModesList
    {
        get { return gameModes; }
        set { gameModes = value; }
    }

    private TypeOfGameMode currentGameMode;

    public TypeOfGameMode CurrentGameMode
    {
        get { return currentGameMode; }
        set { currentGameMode = value; }
    }

   
    // Speed of the game. It goes from 0 to 1
    private float gameSpeed;

    public float GameSpeed
    {
        get { return gameSpeed; }
        set { gameSpeed = value; }
    }

    //-----------------------------------
    // Methods
    //-----------------------------------

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
