﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
*@author Juan Sebastian Baracaldo Angel
*This script controls the music flow between scenes
*/
public class MusicManager : MonoBehaviour
{
    //--------------------------------------
    // Public Constants
    //--------------------------------------

    // Enum for the different game modes
    public enum gameSongs
    {
        song1,
        song2,
        song3
    }
    //-----------------------------------
    // Public Variables
    //-----------------------------------

    //Instance for singleton patern
    public static MusicManager instance;

    //The list of audioclips that corresponds to the gameSongs enum
    //Note that the array lenght must match gameSongs Lenght
    public AudioClip[] audioClips;

    //-----------------------------------
    // Private Variables
    //-----------------------------------
   
    //The current track
    private gameSongs currentTrack;

    //The audioSource destintated to play music
    AudioSource audiosrc;
    //-----------------------------------
    // Methods
    //-----------------------------------

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        

        //Get the audioSourceComponent
        audiosrc = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start ()
    {
        //Set the initial song for the game
        audiosrc.clip = audioClips[(int)gameSongs.song1];
        audiosrc.Play();
	}
	
	//Changes the track of the audiosource destinated to play music
	public void changeTrack (gameSongs newTrack)
    {
        //Check if the track is the same
        if (newTrack == currentTrack)
            return;
        //If not change tracks
        audiosrc.Stop();
        audiosrc.clip = audioClips[(int)newTrack];
        audiosrc.Play();
    }
}
