﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/*
*@author Juan Sebastian Baracaldo Angel
*This Script manages al the navigation between scenes. 
*This also includes music changes if necessary  between scenes
*/

public class MySceneManager : MonoBehaviour
{

    //--------------------------------------
    // Public Constants
    //--------------------------------------

    // Enum for the different scenes of the game
    //Make sure you match these scenes in the same order on File, Build Settings, (AddOpenScenes)
    public enum SceneNames
    {
        scene0,//Scene number 0 in Build settings
        scene1,//Scene number 1 in Build settings
        scene2,//Scene number 2 in Build settings
        scene3,//Scene number 3 in Build settings
        scene4 //Scene number 4 in Build settings

    }
    //-----------------------------------
    // Public Variables
    //-----------------------------------

    //Instance for singleton patern
    public static MySceneManager instance;
    //-----------------------------------
    // Private Variables
    //-----------------------------------

    //The instance of the Game Manager
    GameManager gm;
    //The instance of the music Manager
    MusicManager musicManager;
    //The actual scene

    SceneNames currentScene;
    //-----------------------------------
    // Methods
    //-----------------------------------
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        
    }


    // Use this for initialization
    void Start ()
    {
        //Get the instance of the GM
        gm = GameManager.instance;
        //Get the instance of the Music Manager
        musicManager = MusicManager.instance;
    }

    /*// Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            navigateScenes();
        }
    }*/

    /*
     *Use this method to navigate between scenes
     *Here you do all the routing, if you need more complex conditions you can use the game manager like the example below
     *Also remember to set the songs for each scene right here
     */
    public void navigateScenes()
    {
        //Scene 0 is default scene
        SceneNames newScene = SceneNames.scene0;
        //Set the default track
        MusicManager.gameSongs newSong = MusicManager.gameSongs.song1;
        //Switch between scenes depending on the current scene and current game mode (check game manager)
        switch (currentScene)
        {
            //----------- Case 0 ------------
            case SceneNames.scene0:
                newScene = SceneNames.scene1;
                newSong = MusicManager.gameSongs.song1;
                gm.CurrentGameMode = GameManager.TypeOfGameMode.type2;
                break;
            //----------- Case 1 ------------
            case SceneNames.scene1:
                //Here lets set an example where depending on the current game mode you switch between scenes
                //it's important to remember the relationship with the game manager
                switch (gm.CurrentGameMode)
                {
                    case GameManager.TypeOfGameMode.type1:
                        newScene = SceneNames.scene2;
                        newSong = MusicManager.gameSongs.song2;
                        break;
                    case GameManager.TypeOfGameMode.type2:
                        newScene = SceneNames.scene3;
                        newSong = MusicManager.gameSongs.song2;
                        break;
                    case GameManager.TypeOfGameMode.type3:
                        newScene = SceneNames.scene4;
                        newSong = MusicManager.gameSongs.song3;
                        break;
                    default:
                        newScene = SceneNames.scene2;
                        newSong = MusicManager.gameSongs.song2;
                        break;
                }                
                break;
            //----------- Case 2 ------------
            case SceneNames.scene2:
                newScene = SceneNames.scene3;
                newSong = MusicManager.gameSongs.song2;
                break;
            //----------- Case 3 ------------
            case SceneNames.scene3:
                newScene = SceneNames.scene4;
                newSong = MusicManager.gameSongs.song3;
                break;
            //----------- Case 4 ------------
            case SceneNames.scene4:
                newScene = SceneNames.scene0;
                newSong = MusicManager.gameSongs.song2;
                break;
            //-------- Default Case ---------
            default:
                newScene = SceneNames.scene0;
                newSong = MusicManager.gameSongs.song1;
                break;
        }

        //Load the selected scene
        currentScene = newScene;
        SceneManager.LoadScene((int)newScene);
        //Changes tracks if necesary
        musicManager.changeTrack(newSong);
    }
}
