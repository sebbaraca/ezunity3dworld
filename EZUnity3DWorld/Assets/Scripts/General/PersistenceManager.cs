﻿
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/**
 * This class is in charge of persistance
 * Save and load methods are here
 * To know more about how to Serialize, Save and Load Information check This articles
 * https://blogs.unity3d.com/2014/06/24/serialization-in-unity/
 * https://sometimesicode.wordpress.com/2015/04/11/unity-serialization-part-1-how-it-works-and-examples/
 * https://sometimesicode.wordpress.com/2015/10/22/unity-serialization-part-2-defining-a-serializable-type/
 * https://gamedevelopment.tutsplus.com/tutorials/how-to-save-and-load-your-players-progress-in-unity--cms-20934
 */

public class PersistenceManager : MonoBehaviour
{
    //*********************************************************************
    //Game state to ve saved
    //Here you must put all the information that you want to save
    [System.Serializable]
    public class Game
    {
        //The game speed used in this game
        public float gameSpeed;
        //Scores of the players on the current game
        public int[] playersScore;

        public Game(float speedP, int[] scoresP)
        {
            gameSpeed = speedP;
            playersScore = scoresP;
        }

    }
    //*********************************************************************
    //-----------------------------------
    // Public Variables
    //-----------------------------------
    //The list of saved Games
    public static List<Game> savedGames = new List<Game>();
    //----------------------------------
    // Private Variables
    //----------------------------------

    //------------------------------------
    // Constants
    //------------------------------------

    //----------------------------------------
    // Methods
    //----------------------------------------
    // Use this for initialization
    void Start ()
    {
		
	}
	
	/**
     * This method Saves the current state of the game variables
     * You must ensure that all the information that you want to save is in the Game CLass
     */ 
	public void SaveGameState ()
    {
        savedGames.Add(new Game(GameManager.instance.GameSpeed,ScoreManager.instance.givePlayerScores()));
        BinaryFormatter bf = new BinaryFormatter();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd"); //you can call it anything you want
        bf.Serialize(file, savedGames);
        file.Close();
    }

    /**
     * This mehod Load the saved information and recovers the last saved gameState 
     */ 
     public void LoadGameState()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            savedGames = (List<Game>)bf.Deserialize(file);
            file.Close();
        }
    }
}
