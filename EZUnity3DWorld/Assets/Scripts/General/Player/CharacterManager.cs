﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
*@author Juan Sebastian Baracaldo Angel
 *This class is used to store all character information like skins or models. 
 * If you change available  skins here they change in the whole project (Like a prefab, but with multiple skins or any variable that you need)
 */
public class CharacterManager : MonoBehaviour
{
    //-------------------------------
    // Constants
    //-------------------------------
    //The characterParameters Types
    public enum characterParameter
    {
        Mesh,
        Skin
    };
    //-------------------------------
    // Public Variables
    //-------------------------------
    //The Instance of this manager
    public static CharacterManager instance;
    //List of meshes that your character can have
    public Mesh[] characterMeshes;
    //List of skins that your character can have
    public Material[] characterSkins;

    
   
    //-------------------------------
    //Methods
    //-------------------------------
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);       
    }



    /*
     *Use this method to configure a character visual theme
     *@param charParameter is the parameter you want to change i.e. Mesh or Skin
     *@param parameterId the id in the array of the characterParameter
     *@character the GameObject of the character which contains the parameter you want to change
    */
    public void setCharacterParameter(characterParameter charParameter, int parameterId, GameObject character)
    {
        switch (charParameter)
        {
            case characterParameter.Mesh:
                MeshFilter charMesh = character.GetComponent<MeshFilter>();
                charMesh.mesh = characterMeshes[parameterId];
                break;
            case characterParameter.Skin:
                MeshRenderer charRenderer = character.GetComponent<MeshRenderer>();
                charRenderer.material = characterSkins[parameterId];
                break;
            default:
                break;
        }   
    }
}
