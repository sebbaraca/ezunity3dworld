﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
*@author Juan Sebastian Baracaldo Angel
 *This class is responsible for  any behaviour that the player must have
 *It's recomended that you use inputs defined on the input manager like Input.GetAxis("Horizontal") or Input.GetButtonDown("Jump") 
 *instead of using specific keys like Input.GetKeyDown(KeyCode.A)
 */


public class PlayerController : MonoBehaviour
{
    //-------------------------------
    // Variables
    //-------------------------------
    /*
     *For movement it's recomended that you use public variables like speed, so you can tune easily your movement in the inspector
     */
    public float speed;
    //-------------------------------
    // Methods
    //-------------------------------

    // Use this for initialization
    void Start () {
        
	}
	
	/*
     *It's also recomended that any behaviour that involves physics is placed inside the fixed update, so for example if we got player movement, it would be placed inside this method
     */
	void FixedUpdate ()
    {

        //An example of movement
        transform.Translate(Vector3.right * Input.GetAxis("Horizontal")*speed + Vector3.forward * speed * Input.GetAxis("Vertical"), Space.Self);
    }
}
