﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
*@author Juan Sebastian Baracaldo Angel
*This class must control the score of the players and if necesary display it on UI
*/

public class ScoreManager : MonoBehaviour
{

    //-----------------------------------
    // Public Variables
    //-----------------------------------

    
    // Text list for display of the score
    public Text[] p_text;

  
    //The instance of the ScoreManager
    public static ScoreManager instance;
    
    //----------------------------------
    // Private Variables
    //----------------------------------
    
        // The current score for players
    private int[] currentScore;
    
        //The id of the player who is winning
    private GameManager.PlayerId currentWinner;
    //The highest score
    private int currentWinnerScore;

    
    //delagate to change crown
   public delegate void ChangeAction(GameManager.PlayerId player);
    public static event ChangeAction OnChanged;
 

    //------------------------------------
    // Constants
    //------------------------------------

    AudioSource src;

    //----------------------------------------
    // Methods
    //----------------------------------------

    // Method used when the game has started
    void Awake()
    {
        if (instance != null && instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        instance = this;
        //DontDestroyOnLoad(gameObject);
        currentWinner = GameManager.PlayerId.Player1;
        currentWinnerScore = 0;
    }

    // Method used when the game has started
    void Start()
    {

        for (int i = 0; i < currentScore.Length; i++)
        {
            currentScore[i] = 0;
        }
            
        checkForGameMode();

    }

   
    // Method which gives the winner of the game
    public GameManager.PlayerId giveWinner()
    {
        //Check who has the most points
        int maximumScore = 0;
        int num = 0;
        for (int i = 0; i < currentScore.Length; i++)
        {
            if (maximumScore == currentScore[i])
            {
                num++;
            }
            if (currentScore[i]>maximumScore)
            {
                maximumScore = currentScore[i];
                currentWinner = (GameManager.PlayerId)i;
            }
            
        } 
      

        if (num > 1)
        {
           //Draw
            currentWinner = GameManager.PlayerId.Default;
        }

        return currentWinner;
    }


    /*
    *Depending on the game mode you may do a different thing with the score
    *I sugest using delegates. For more information you can check this Unity tutorial https://unity3d.com/learn/tutorials/topics/scripting/delegates
    *if you don't understand this you can always use the singleton of this class and make the update Score method public
    */

    public void checkForGameMode()
    {

        switch (GameManager.instance.CurrentGameMode)
        {
            case GameManager.TypeOfGameMode.type1:
                //WinningPlatform.OnTouched += UpdateScore; //This is an example on how to subscribe a Delegate
                break;
            case GameManager.TypeOfGameMode.type2:
                break;
            case GameManager.TypeOfGameMode.type3:
                break;
            default:
                break;
        }
    }
    /*
    *This method is important if you used the Delegate system sugested on checkForGameMode().
    *Always unsubscribe before navigating between scenes
    */
    public void Unsuscribe()
    {

        //Unuscribe Service
        switch (GameManager.instance.CurrentGameMode)
        {
            case GameManager.TypeOfGameMode.type1:
                //WinningPlatform.OnTouched -= UpdateScore; //This is an example on how to unsubscribe a Delegate
                break;
            case GameManager.TypeOfGameMode.type2:
                break;
            case GameManager.TypeOfGameMode.type3:
                break;
            default:
                break;
        }

    }

  //This method updates the score for a given player
  //@param player is the PlayerId of the player's score
  //@param score is the increment in the score
    private void UpdateScore(GameManager.PlayerId player, int score)
    {       
        currentScore[(int)player] += score;
        if (currentScore[(int)player] > currentWinnerScore)
        {
            currentWinner = player;
            currentWinnerScore = currentScore[(int)player];
            OnChanged(currentWinner);
        }
              
        

        //Set the Score on the Canvas
       
        p_text[(int)player].text = "" + currentScore ;
        
    }


    /**
     * Returns the current score of the players
     */
     public int[] givePlayerScores()
    {
        return currentScore;
    } 
}
