﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
*@author Juan Sebastian Baracaldo Angel
*This class is the parent of all lvl manager
*It is responsible of initialization, ending and reset of all lvls
*It initializes players with the characteristics for each lvl
*/
public abstract class LvlManagerGeneral : MonoBehaviour
{
    //-----------------------------------
    // Public Variables
    //-----------------------------------
    //The GO of the players
    public GameObject[] players;
    //the ids of the skins and meshes to be used for each player
    //This is must match with the id used in the characterManager
    public int[] skinsId;
    public int[] meshId;
    //-----------------------------------
    // Methods
    //-----------------------------------
    // Use this for initialization
    void Start ()
    {
      
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    //This method initializes the players in the current lvl
    //Initializes skins and meshes
   public void initializePlayers()
    {
        for (int i = 0; i < players.Length; i++)
        {
            
            CharacterManager.instance.setCharacterParameter(CharacterManager.characterParameter.Mesh, meshId[i], players[i]);
            CharacterManager.instance.setCharacterParameter(CharacterManager.characterParameter.Skin, skinsId[i], players[i]);
        }       

    }
    //-----------------------------------
    // Abstract Methods
    //-----------------------------------
    //Use this method to initialize the lvl
    public abstract void initializeLvl();

    //Use this method the end a lvl
    public abstract void endLvl();
}
