﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlManager_type1 : LvlManagerGeneral {
    

    // Use this for initialization
    void Start () {
        //Initialize te game
        initializeLvl();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    //Ends the lvl
    public override void endLvl()
    {
        MySceneManager.instance.navigateScenes();
    }
    //Starts lvl
    public override void initializeLvl()
    {
        initializePlayers();
    }
}
